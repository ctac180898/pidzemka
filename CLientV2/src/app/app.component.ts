import { Component, Inject } from '@angular/core';
import { AuthService } from './services/auth.service';
import {MatDialog, MatDialogConfig} from '@angular/material';
import { LoginDialogComponent } from './components/login-dialog/login-dialog.component';
import { SearchDialogComponent} from './components/search-dialog/search-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  private _authSubscription;

  constructor( public auth: AuthService, public dialog: MatDialog ) {
    this._authSubscription = this.auth.Auth().authState.subscribe(authState => {
      if (authState) {
        this.closeDialog();
        return;
      }
    });
  }

  openSearchDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.hasBackdrop = true;
    dialogConfig.backdropClass = 'search-dialog-backdrop'
    dialogConfig.panelClass = 'search-dialog';
    this.dialog.open(SearchDialogComponent, dialogConfig);
  }

  openLoginDialog() {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    dialogConfig.hasBackdrop = true;
    dialogConfig.panelClass = 'login-dialog-container';

    this.dialog.open(LoginDialogComponent, dialogConfig);
  }

  closeDialog() {
    this.dialog.closeAll();
  }

}
