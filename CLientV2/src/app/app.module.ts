import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// DESIGN
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import {MatDialog, MatDialogConfig, MatDialogModule} from '@angular/material';
import { MatTabsModule } from '@angular/material';
import { MatStepperModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { MatGridListModule } from '@angular/material';
import { MatMenuModule } from '@angular/material';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatSelectModule} from '@angular/material/select';


// EDITOR
import { EditorModule } from '@tinymce/tinymce-angular';

// FIREBASE
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireStorageModule } from 'angularfire2/storage';

// App
import { AppComponent } from './app.component';
import { EditorComponent } from './components/editor/editor.component';
import { IndexComponent } from './components/index/index.component';
import { LoginDialogComponent } from './components/login-dialog/login-dialog.component';
import { appRoutes } from './app.router';
import { environment } from '../environments/environment';
import { AuthorsComponent } from './components/authors/authors.component';
import { ArticleComponent } from './components/article/article.component';
import { SafeHtmlPipe } from './safe-html.pipe';
import { WeeksComponent } from './components/weeks/weeks.component';
import { SearchDialogComponent } from './components/search-dialog/search-dialog.component';
import { NavButtonDirective } from './directives/nav-button.directive';
import { NavButtonDropDirective } from './directives/nav-button-drop.directive';
import { WeekComponent } from './components/week/week.component';
import { RubricComponent } from './components/rubric/rubric.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { SpecialProjectsComponent } from './components/special-projects/special-projects.component';
import { DonateComponent } from './components/donate/donate.component';
import { SearchComponent } from './components/search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    EditorComponent,
    IndexComponent,
    LoginDialogComponent,
    AuthorsComponent,
    ArticleComponent,
    SafeHtmlPipe,
    WeeksComponent,
    SearchDialogComponent,
    NavButtonDirective,
    NavButtonDropDirective,
    WeekComponent,
    RubricComponent,
    AboutUsComponent,
    SpecialProjectsComponent,
    DonateComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    ReactiveFormsModule,
    EditorModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
    AngularFireStorageModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatDividerModule,
    MatDialogModule,
    MatTabsModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatGridListModule,
    MatMenuModule,
    MatSelectModule,
    NgbModule.forRoot()
  ],
  providers: [MatDialogConfig],
  bootstrap: [AppComponent],
  entryComponents: [LoginDialogComponent, SearchDialogComponent]
})
export class AppModule { }
