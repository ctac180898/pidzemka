import { Routes } from '@angular/router';
import { IndexComponent } from './components/index/index.component';
import { EditorComponent } from './components/editor/editor.component';
import { CanEditGuard } from './services/can-edit.guard';
import { AuthorsComponent } from './components/authors/authors.component';
import {ArticleComponent} from './components/article/article.component';
import {WeeksComponent} from './components/weeks/weeks.component';
import {WeekComponent} from './components/week/week.component';
import {RubricComponent} from './components/rubric/rubric.component';
import {AboutUsComponent} from './components/about-us/about-us.component';
import {SpecialProjectsComponent} from './components/special-projects/special-projects.component';
import {DonateComponent} from './components/donate/donate.component';
import {SearchComponent} from './components/search/search.component';

export const appRoutes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'editor', component: EditorComponent, canActivate: [CanEditGuard] },
  { path: 'editor/:title', component: EditorComponent, canActivate: [CanEditGuard] },
  { path: 'authors', component: AuthorsComponent },
  { path: 'weeks', component: WeeksComponent},
  { path: 'article/:title', component: ArticleComponent },
  { path: 'week/:name', component: WeekComponent},
  { path: 'rubric/:name', component: RubricComponent},
  { path: 'about-us', component: AboutUsComponent},
  { path: 'special-projects', component: SpecialProjectsComponent},
  { path: 'donate', component: DonateComponent},
  { path: 'search/:param', component: SearchComponent}
];
