import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Article } from '../../models/article.model';
import { ArticleService } from '../../services/article.service';
import { AuthService } from '../../services/auth.service';
import {ArticleComment} from '../../models/comment.model';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {LoginDialogComponent} from '../login-dialog/login-dialog.component';
import {Observable} from 'rxjs/Observable';
import {UserModel} from '../../models/user.model';
import {concat} from 'rxjs-compat/operator/concat';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
  // encapsulation: ViewEncapsulation.None,
})
export class ArticleComponent implements OnInit {
  editor = false;
  author$;
  article;
  title;
  author;
  article$;
  comments$: Observable<ArticleComment[]>;
  users = [];
  commentText = '';
  commentsLength;
  errorMessage = 'Занадто довгий коментар';
  constructor(private Activatedroute: ActivatedRoute,
              private router: Router,
              private auth: AuthService,
              private articleService: ArticleService,
              public dialog: MatDialog ) {
  }

  ngOnInit() {
    this.title = this.Activatedroute.snapshot.params['title'];
    this.article$ = this.articleService.getArticleByTitle(this.title);
    this.article$.subscribe(article => {
      this.article = article;
      console.log(article);
      this.author$ = this.articleService.GetAuthor(article.authorName);
      this.author$.subscribe(author => {
        this.author = author;
      });
      this.comments$ = this.articleService.getArticleComments(this.title);
      this.comments$.subscribe(comments => {
        this.commentsLength = comments.length;
        comments.forEach((comment) => {
          this.getUser(comment.userId);
        });
      });
    });
  }

  notValidComment(): boolean {
    return this.commentText.length > 500;
  }

  authenticated() {
    return this.auth.authenticated;
  }

  getUser(id) {
    const user$ = this.auth.getUser(id);
    user$.subscribe(user => {
      this.users.push(user);
    });
  }

  openLoginDialog() {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    dialogConfig.hasBackdrop = true;
    dialogConfig.panelClass = 'login-dialog-container';

    this.dialog.open(LoginDialogComponent, dialogConfig);
  }

  addComment() {
    const comment = {
      userId: this.auth.currentUserId,
      createdAt: new Date(),
      content: this.commentText
    };
    this.articleService.addComment(comment, this.article);
  }
}
