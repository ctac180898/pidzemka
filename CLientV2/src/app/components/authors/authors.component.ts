import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  isDataAvailable = false;
  authors;
  authors$;
  constructor(public auth: AuthService) { }

  ngOnInit() {
    this.authors$ = this.auth.GetAuthors();
    this.authors$.subscribe(authors => {
      this.authors = authors;
      this.isDataAvailable = true;
    });
  }

}
