import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Article } from '../../models/article.model';
import { ArticleService } from '../../services/article.service';
import {Week} from '../../models/week.model';
import {Author} from '../../models/author.model';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {

  toEditTitle;
  weeks$;
  content: string;
  artImage: string;
  tags: string;
  rubric: string;
  article: Article;
  title: string;
  errorMessage: string;
  articleWeek: string;
  weekName: string;
  authors$: Observable<Author[]>;
  weekImg: string;
  weekDeskr: string;
  authorDisplayName: string;
  authorFbLink: string;
  authorImg: string;
  authorName: string;
  loadEditor = false;
  tabIndex;
  constructor(public auth: AuthService,
              public articleService: ArticleService,
              private Activatedroute: ActivatedRoute,
              private  router: Router) {
    this.Activatedroute.params.subscribe(
      params => {
        const toEditTitle = this.Activatedroute.snapshot.params['title'];
        this.router.navigate([`/editor/${toEditTitle}`]);
      });
  }

  ngOnInit() {
    this.toEditTitle = this.Activatedroute.snapshot.params['title'];
    if (this.toEditTitle && this.toEditTitle !== 'undefined') {
      this.articleService.getArticleByTitle(this.toEditTitle).subscribe(article => {
        this.content = article.content;
        this.rubric = article.rubric;
        this.artImage = article.img;
        this.title = article.title;
        this.articleWeek = article.week;
        this.loadEditor = true;
      });
    } else {
      this.loadEditor = true;
    }
    this.authors$ = this.articleService.getAuthors();
    this.weeks$ = this.articleService.getNotFullWeeksList();
  }

  checkEmpty() {
    return this.title && this.artImage && this.rubric && this.content && this.tags && this.articleWeek;
  }

  addArticle() {
    console.log(this.articleWeek);
    if (!this.checkEmpty()) {
      this.errorMessage = 'Будь ласка, заповніть усі поля';
      return;
    }
    if (!this.auth.authenticated) {
      this.errorMessage = 'Хз чому, але залогіньтесь пліз';
    }
    const tagsArray = this.tags.split(' ');
    const tagsObj = {};
    tagsArray.forEach((tag) => {
      tagsObj[tag] = true;
    });
    this.article = {
      content: this.content,
      title: this.title,
      img: this.artImage,
      rubric: this.rubric,
      authorName: this.authorName,
      createdAt: new Date(),
      updatedAt: new Date(),
      tags: tagsObj,
      week: this.articleWeek,
    };
    console.log(this.article);

    this.articleService.createArticle(this.article);
  }

  AddWeek() {
    const week: Week = {
      deskr: this.weekDeskr,
      name: this.weekName,
      img: this.weekImg,
      articles: 0,
    };
    this.articleService.createWeek(week);
  }

  AddAuthor() {
    const author: Author = {
      name: this.authorDisplayName,
      img: this.authorImg,
      facebook: this.authorFbLink
    };
    this.articleService.createAuthor(author);
  }

  unloadTinyMce() {
    this.loadEditor = false;
  }

  updateEditor() {
    if (this.tabIndex === 0) {
      this.loadEditor = true;
    }
  }
}
