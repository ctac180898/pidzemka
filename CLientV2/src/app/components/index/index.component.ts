import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../../services/article.service';
import {Article} from '../../models/article.model';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {Week} from '../../models/week.model';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  width;
  weeks$: Observable<Week[]>;
  tiles = [
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
  ];

  gutter: number;
  articles$: Observable<Article[]>;

  public breakpoint: number;

  constructor( public articleService: ArticleService, private router: Router) {
    const query = {
      path: 'articles',
      whereFilter: false,
      orderByField: 'createdAt',
      limit: 10,
      descending: true,
      previous: false,
      whereField: ''
    };
    this.articleService.init(query);
    this.gutter = 20;
    this.breakpoint = 3;
    this.width = 1600;
    if (window.innerWidth <= 1600) {
      this.width = window.innerWidth - 20;
      this.breakpoint = 2;
      if (window.innerWidth <= 1280) {
        this.width = window.innerWidth - 20;
      }
    }
    if (window.innerWidth <= 950) {
      this.width = window.innerWidth - 20;
      this.breakpoint = 1;
    }
  }

  nextPage() {
    this.articleService.setPrev(false);
    this.articleService.setDesc(true);
    this.articleService.page();
  }

  prevPage() {
    this.articleService.setPrev(true);
    this.articleService.setDesc(false);
    this.articleService.page();
  }

  ngOnInit() {
    this.articles$ = this.articleService.data;
    this.weeks$ = this.articleService.getWeeksList();
  }

  onResize(event) {
    if (event.target.innerWidth <= 1600) {
      this.width = window.innerWidth - 20;
      this.breakpoint = 2;
      if (event.target.innerWidth <= 1280) {
        this.width = window.innerWidth - 20;
      }
    }
    if (window.innerWidth <= 950) {
      this.width = window.innerWidth - 20;
      this.breakpoint = 1;
    }
    if (event.target.innerWidth > 1600) {
      this.width = 1600;
      this.breakpoint = 3;
    }
  }

  randomArticle() {
    const weeks$ = this.articleService.getWeeksList();
    weeks$.subscribe(weeks => {
      const weekNumber = Math.floor(Math.random() * (weeks.length));
      const weekName = weeks[weekNumber].name;
      this.articleService.getArticlesByWeek(weekName).subscribe(articles => {
          const articleNumber = Math.floor(Math.random() * (articles.length));
          this.router.navigate(['/article', articles[articleNumber].title]);
        }
      );
    });
  }

}
