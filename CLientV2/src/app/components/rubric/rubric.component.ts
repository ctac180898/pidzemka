import {Component, OnInit} from '@angular/core';
import {ArticleService} from '../../services/article.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {MatDialog} from '@angular/material';
import {Article} from '../../models/article.model';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-rubric',
  templateUrl: './rubric.component.html',
  styleUrls: ['./rubric.component.scss']
})
export class RubricComponent implements OnInit {

  rubric;
  notFound = false;

  width;
  tiles = [
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
  ];

  gutter: number;
  articles$: Observable<Article[]>;

  public breakpoint: number;

  constructor(private router: Router,
              private Activatedroute: ActivatedRoute,
              public articleService: ArticleService ) {
    this.Activatedroute.params.subscribe(
      params => {
        this.ngOnInit();
      });
  }

  nextPage() {
    this.articleService.setPrev(false);
    this.articleService.setDesc(true);
    this.articleService.page();
  }

  prevPage() {
    this.articleService.setPrev(true);
    this.articleService.setDesc(false);
    this.articleService.page();
  }

  ngOnInit() {
    console.log('oninit');
    this.notFound = false;
    this.rubric = this.Activatedroute.snapshot.params['name'];
    const query = {
      path: 'articles',
      whereFilter: true,
      orderByField: 'createdAt',
      limit: 9,
      descending: true,
      previous: false,
      whereField: 'rubric',
      whereFiledValue: this.rubric
    };
    this.articleService.init(query);
    this.gutter = 20;
    this.breakpoint = 3;
    this.width = 1600;
    if (window.innerWidth <= 950) {
      this.width = window.innerWidth - 50;
      this.tiles[4].cols = 1;
      this.tiles[4].rows = 1;
      this.tiles[7].cols = 1;
      this.tiles[7].rows = 1;
      this.breakpoint = 1;
    }
    this.articles$ = this.articleService.data;
    this.articles$.subscribe(articles => {
      if (articles.length === 0) {
        this.notFound = true;
      } else {
        this.notFound = false;
      }
    });
  }

  onResize(event) {
    if (event.target.innerWidth <= 950) {
      this.width = event.target.innerWidth - 50;
      this.tiles[4].cols = 1;
      this.tiles[4].rows = 1;
      this.tiles[7].cols = 1;
      this.tiles[7].rows = 1;
      this.breakpoint = 1;
    }
    if (event.target.innerWidth > 950) {
      this.width = 950;
      this.tiles[4].cols = 2;
      this.tiles[4].rows = 2;
      this.tiles[7].cols = 2;
      this.tiles[7].rows = 2;
      this.breakpoint = 3;
    }
  }

}
