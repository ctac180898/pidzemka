import { Component, OnInit } from '@angular/core';
import {Article} from '../../models/article.model';
import {Observable} from 'rxjs/Observable';
import {ArticleService} from '../../services/article.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  param;
  notFound = false;

  width;
  tiles = [
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 2, rows: 2},
    {cols: 1, rows: 1},
    {cols: 1, rows: 1},
    {cols: 2, rows: 2},
  ];

  gutter: number;
  articles$: Observable<Article[]>;
  public breakpoint: number;

  constructor(private router: Router,
              private Activatedroute: ActivatedRoute,
              public articleService: ArticleService) {
    this.Activatedroute.params.subscribe(
      params => {
        this.ngOnInit();
      });
  }

  nextPage() {
    this.articleService.setPrev(false);
    this.articleService.setDesc(true);
    this.articleService.page();
  }

  prevPage() {
    this.articleService.setPrev(true);
    this.articleService.setDesc(false);
    this.articleService.page();
  }

  ngOnInit() {
    this.notFound = false;
    this.param = this.Activatedroute.snapshot.params['param'];
    const query = {
      path: 'articles',
      whereFilter: true,
      orderByField: 'createdAt',
      limit: 9,
      descending: true,
      previous: false,
      whereField: `tags.${this.param}`,
      whereFiledValue: true
    };
    this.articleService.init(query);
    this.gutter = 20;
    this.breakpoint = 2;
    this.width = 941;
    if (window.innerWidth <= 950) {
      this.width = window.innerWidth - 50;
      this.tiles[4].cols = 1;
      this.tiles[4].rows = 1;
      this.tiles[7].cols = 1;
      this.tiles[7].rows = 1;
      this.breakpoint = 1;
    }
    this.articles$ = this.articleService.data;
    this.articles$.subscribe(articles => {
      if (articles.length === 0) {
        this.notFound = true;
      } else {
        this.notFound = false;
      }
    });
  }

  onResize(event) {
    if (event.target.innerWidth <= 950) {
      this.width = event.target.innerWidth - 50;
      this.tiles[4].cols = 1;
      this.tiles[4].rows = 1;
      this.tiles[7].cols = 1;
      this.tiles[7].rows = 1;
      this.breakpoint = 1;
    }
    if (event.target.innerWidth > 950) {
      this.width = 950;
      this.tiles[4].cols = 2;
      this.tiles[4].rows = 2;
      this.tiles[7].cols = 2;
      this.tiles[7].rows = 2;
      this.breakpoint = 2;
    }
  }

}
