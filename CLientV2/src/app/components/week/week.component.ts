import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {ArticleService} from '../../services/article.service';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-week',
  templateUrl: './week.component.html',
  styleUrls: ['./week.component.scss']
})
export class WeekComponent implements OnInit {

  name;
  week$;
  articles$;
  week;
  constructor(private Activatedroute: ActivatedRoute,
              private router: Router,
              private auth: AuthService,
              private artServ: ArticleService ) { }

  ngOnInit() {
    this.name = this.Activatedroute.snapshot.params['name'];
    this.week$ = this.artServ.getWeekByName(this.name);
    this.week$.subscribe(week => {
      this.week = week;
      this.articles$ = this.artServ.getArticlesByWeek(this.week.name);
      this.articles$.subscribe(articles => {
        console.log(articles);
      });
    });
  }

}
