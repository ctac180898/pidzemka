import {Component, ElementRef, HostListener, OnInit} from '@angular/core';
import {ArticleService} from '../../services/article.service';
import {Observable} from 'rxjs/Observable';
import {Week} from '../../models/week.model';

@Component({
  selector: 'app-weeks',
  templateUrl: './weeks.component.html',
  styleUrls: ['./weeks.component.scss']
})
export class WeeksComponent implements OnInit {

  isWeeksReady = false;
  weeks$: Observable<Week[]>;

  constructor(private articleService: ArticleService, private el: ElementRef) {

  }


  ngOnInit() {
    this.isWeeksReady = false;
    this.weeks$ = this.articleService.getWeeksList();
  }

}
