import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[appNavButtonDrop]'
})
export class NavButtonDropDirective {

  constructor(private el: ElementRef) {
    el.nativeElement.style.color = 'black';
    el.nativeElement.style.fontFamily = 'B52';
    el.nativeElement.style.fontSize = '17px';
    // el.nativeElement.style.backgroundColor = 'rgba( 10.6, 10.6, 10.6, 0)';
    el.nativeElement.style.transition = 'background-color .3s ease-in-out';
  }

  /*@HostListener('mouseenter') onMouseEnter() {
    this.setColor('#00838F');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.setColor('rgba( 10.6, 10.6, 10.6, 0)');
  }

  setColor(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }*/
}
