import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[appNavButton]'
})
export class NavButtonDirective {

  constructor(private el: ElementRef) {
    el.nativeElement.style.marginRight = '20px';
    el.nativeElement.style.marginBottom = '5px';
    el.nativeElement.style.color = 'black';
    el.nativeElement.style.fontFamily = 'B52';
    el.nativeElement.style.fontSize = '17px';
    // el.nativeElement.style.backgroundColor = '#1B1B1B';
    el.nativeElement.style.transition = 'background-color .3s ease-in-out';
  }

  /*@HostListener('mouseenter') onMouseEnter() {
    this.setColor('#00838F');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.setColor('#FFFFFF');
  }

  setColor(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }*/

}
