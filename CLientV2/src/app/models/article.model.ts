export interface Article {
  content: string;
  authorName: string;
  img: string;
  createdAt: Date;
  updatedAt: Date;
  tags: {};
  rubric: string;
  title: string;
  week: string;
  comments?: Comment[];
}
