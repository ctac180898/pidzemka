export interface Author {
  id?: string;
  facebook: string;
  name: string;
  img: string;
}
