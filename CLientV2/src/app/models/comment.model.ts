export interface ArticleComment {
  userId: string;
  content: string;
  createdAt: Date;
}
