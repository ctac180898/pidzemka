export interface QueryConfig {
  path: string; //  path to collection
  orderByField: string; // field to orderBy
  whereFilter: boolean;
  whereField: string; // field to where query
  whereFiledValue: string;
  limit: number; // limit per query
  descending: boolean;
  previous: boolean;
}
