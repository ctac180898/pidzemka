export interface Roles {
  subscriber?: boolean;
  editor?: boolean;
  admin?: boolean;
}

export interface UserModel {
  uid: string;
  email: string;
  roles: Roles;
  photoURL: string;
  displayName: string;
}
