export interface Week {
  name: string;
  img: string;
  deskr: string;
  articles: number;
}
