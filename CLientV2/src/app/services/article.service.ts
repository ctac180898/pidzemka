import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import { Article } from '../models/article.model';
import {Observable} from 'rxjs/Observable';
import {Week} from '../models/week.model';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/scan';
import 'rxjs/add/operator/take';
import {QueryConfig} from '../models/query-config.model';
import {Author} from '../models/author.model';
import {Tag} from '../models/tag.model';
import {ArticleComment} from '../models/comment.model';
import {from} from 'rxjs/internal/observable/from';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class ArticleService {

  private _done = new BehaviorSubject(false);
  public done = new Observable<boolean>();
  private _loading = new BehaviorSubject(false);
  private prev: boolean;
  private _data = new BehaviorSubject([]);
  private query: QueryConfig;
  private counter: number;
  public nextPageExists: boolean;
  public data: Observable<Article[]>;

  userId: string;

  constructor(private afs: AngularFirestore, private afAuth: AngularFireAuth, private router: Router) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userId = user.uid;
      }
    });
  }

  init(query) {
    this._done.next(false);
    this.prev = false;
    this.counter = 0;
    this.query = query;

    const first = this.afs.collection(this.query.path, ref => {
      return this.query.whereFilter ?
        ref
        .where(this.query.whereField, '==', this.query.whereFiledValue)
        .orderBy(this.query.orderByField, this.query.descending ? 'desc' : 'asc')
        .limit(this.query.limit) :
        ref
        .orderBy(this.query.orderByField, this.query.descending ? 'desc' : 'asc')
        .limit(this.query.limit);
    });

    this.mapAndUpdate(first);

    // Create the observable array for consumption in components
    this.data = this._data.asObservable()
      .scan( (acc, val) => {
        return val;
      });
    this.done = this._done.asObservable()
      .scan( (acc, val) => {
        return acc;
      });
  }

  // Retrieves additional data from firestore
  page() {
    const cursor = this.getCursor(this.query.descending);
    const more = this.afs.collection(this.query.path, ref => {
      return this.query.whereFilter ?
        ref
        .where(this.query.whereField, '==', this.query.whereFiledValue)
        .orderBy(this.query.orderByField, this.query.descending ? 'desc' : 'asc')
        .limit(this.query.limit)
        .startAfter(cursor) :
        this.query.previous ?
          ref
          .orderBy(this.query.orderByField, this.query.descending ? 'desc' : 'asc')
          .limit(this.query.limit)
          .startAt(cursor) :
          ref
            .orderBy(this.query.orderByField, this.query.descending ? 'desc' : 'asc')
            .limit(this.query.limit)
            .startAfter(cursor);
    });
    this.mapAndUpdate(more);
    this.query.previous ? this.counter-- : this.counter++;
  }

  setPrev(val) {
    this.query.previous = val;
  }

  setDesc(val) {
    this.query.descending = val;
  }

  public GetPaginateCounter(): number {
    return this.counter;
  }

  // Determines the doc snapshot to paginate query
  private getCursor(direction) {
    const current = this._data.value;
    if (current.length) {
      return direction ? current[current.length - 1].doc : current[0].doc;
    }
    return null;
  }

  // Maps the snapshot to usable format the updates source
  private mapAndUpdate(col: AngularFirestoreCollection<any>) {

    if (this._done.value && !this.query.previous) {
      return;
    }
    // loading
    this._loading.next(true);

    // Map snapshot with doc ref (needed for cursor)
    return col.snapshotChanges()
      .do(arr => {
        let values = arr.map(snap => {
          const data = snap.payload.doc.data();
          const doc = snap.payload.doc;
          return { ...data, doc };
        });
        if (values.length === 0 && this.counter === 0) {
          this.router.navigate(['rubric/notfound']);
        }

        if (values.length < 10) {
          this.nextPageExists = false;
        } else {
          this.nextPageExists = true;
        }

        this.query.previous ? values = values.slice(1, 10) : values = values.slice(0, 9);

        values = this.query.previous ? values.reverse() : values;
        // update source with new values, done loading
        this._data.next(values);
        this._loading.next(false);

        // no nextPage values, mark done
      })
      .take(8)
      .subscribe();
  }

  getArticlesByWeek(week): Observable<Article[]> {
    return this.afs.collection<Article>('articles',
        ref => ref
      .where('week', '==', week)
          .limit(3))
      .valueChanges();
  }

  getArticlesByRubric(rubric): Observable<Article[]> {
    return this.afs.collection<Article>('articles', ref => ref.where('rubric', '==', rubric)).valueChanges();
  }

  getArticleByTitle(title): Observable<Article> {
    return this.afs.doc<Article>(`articles/${title}`).valueChanges();
  }

  createArticle(item: Article) {
    this.updateWeek(item.week);
    this.afs.doc<Article>(`articles/${item.title}`).set(item);
  }

  updateArticle(item: Article) {
    this.afs.doc<Article>(`articles/${item.title}`).update(item);
  }

  getArticlesByTag(tag): Observable<Article[]> {
    return this.afs.collection<Article>('articles', ref => ref.where(tag, '==', true)).valueChanges();
  }

  createWeek(item: Week) {
    this.afs.doc<Week>(`weeks/${item.name}`).set(item);
  }

  getWeekByName(name): Observable<Week> {
    return this.afs.doc<Week>(`weeks/${name}`).valueChanges();
  }

  getWeeksList(): Observable<Week[]> {
    return this.afs.collection<Week>('weeks').valueChanges();
  }

  updateWeek(name) {
    this.afs.doc<Week>(`weeks/${name}`).ref.get().then(week => {
      const newWeek = week.data();
      newWeek.articles++;
      this.afs.doc<Week>(`weeks/${name}`).update(newWeek);
    });
  }

  getNotFullWeeksList(): Observable<Week[]> {
    return this.afs.collection<Week>('weeks', ref => ref.where('articles', '<', 3)).valueChanges();
  }

  createAuthor(item: Author) {
    this.afs.doc<Author>(`authors/${item.name}`).set(item);
  }

  getAuthors() {
    return this.afs.collection<Author>('authors').valueChanges();
  }

  public GetAuthor(name: string): Observable<Author> {
    return this.afs.doc<Author>(`authors/${name}`).valueChanges();
  }

  getAuthorByFacebook(fb) {
    return this.afs.collection<Author>('authors', ref => ref.where('facebook', '==', fb)).valueChanges();
  }

  addComment(comment, article) {
    this.afs.doc<Article>(`articles/${article.title}`).collection<ArticleComment>('comments').add(comment);
  }

  getArticleComments(title): Observable<ArticleComment[]> {
    return this.afs.doc<Article>(`articles/${title}`)
      .collection<ArticleComment>('comments', ref => ref
        .orderBy('createdAt', 'desc')).valueChanges();
  }

  createTag(item) {
    this.afs.doc<Tag>(`tags/${item.name}`).set(item);
  }

  getTags() {
    return this.afs.collection<Tag>('tags').valueChanges();
  }
}
