import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import {AngularFirestore, AngularFirestoreDocument} from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import {Roles, UserModel} from '../models/user.model';
import {Article} from '../models/article.model';
import { tap, map, take } from 'rxjs/operators';
import 'rxjs/add/observable/of';
import {Author} from '../models/author.model';
import {from} from 'rxjs/internal/observable/from';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  user$: Observable<UserModel>;
  authState: any = null;
  editor = false;

  constructor(private afAuth: AngularFireAuth,
              private afs: AngularFirestore,
              private router: Router) {
    //// Get auth data, then get firestore user document || null
    this.afAuth.authState
      .subscribe(user => {
        if (user) {
          this.user$ =  this.afs.doc<UserModel>(`users/${user.uid}`).valueChanges();
          this.user$.subscribe(usdata => {
            if (usdata.roles.editor) {
              this.editor = usdata.roles.editor;
            } else {
              this.editor = false;
            }
          });
        } else {
          return null;
        }
      });
    this.afAuth.authState.subscribe((auth) => {
      this.authState = auth;
    });
  }

  ///// Login/Signup //////
  Auth() {
    return this.afAuth;
  }

  get authenticated(): boolean {
    return this.authState !== null;
  }

  get currentUserId(): string {
    return this.authenticated ? this.authState.uid : '';
  }

  GetAuthors(): Observable<UserModel[]> {
    return this.afs.collection<UserModel>('users', ref => ref.where('editor', '==', true)).valueChanges();
  }

  googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  getUser(id) {
    return this.afs.doc<UserModel>(`users/${id}`).valueChanges().take(1);
  }

  private oAuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
      .then((credential) => {
        this.updateUserData(credential.user);
      });
  }

  signOut() {
    this.editor = false;
    this.afAuth.auth.signOut();
  }

  private updateUserData(user) {
    // Sets user data to firestore on login
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const data: UserModel = {
      uid: user.uid,
      displayName: user.displayName,
      email: user.email,
      roles: {
        subscriber: true
      },
      photoURL: user.photoURL
    };
    return userRef.ref.set( data, { merge: true } );
  }

  canEdit(): Observable<boolean> {
    const allowedRoles = ['admin', 'editor'];
    return this.checkForAllowed(allowedRoles);
  }

  private checkForAllowed(allowedRoles: string[]): Observable<boolean> {
    if (!this.authenticated) {
      return Observable.of(false);
    }
    const userObs = this.afs.doc<UserModel>(`users/${this.currentUserId}`).valueChanges();
     return userObs.pipe(
      take(1),
      map( user => {
        for (const role of allowedRoles) {
          if ( user.roles[role]) {
            return true;
          }
        }
        return false;
      })
    );
  }
}
