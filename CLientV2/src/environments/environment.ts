// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  firebase: {
    apiKey: 'AIzaSyDwqKT2AuvDUpD8L4hJYCwHo8jOd_4UrOo',
    authDomain: 'pidzemka-11303.firebaseapp.com',
    databaseURL: 'https://pidzemka-11303.firebaseio.com',
    projectId: 'pidzemka-11303',
    storageBucket: 'pidzemka-11303.appspot.com',
    messagingSenderId: '15090272779'
  },
  production: false
};
